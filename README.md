# Construct Activation Patterns

## Notes from 2018-03-13, meeting with Samvel, Rik & Gjalt-Jorn

Take a number of target beliefs (e.g. ~ 10); some with high centrality, some with low.

In each trial, prompt a participant with a belief; then prompt ~ 5 minutes of free association. Record & transcribe data.

Either do it fully between-subjects, or use a mixed (both between and within-subjects) design. If using a mixed design, make sure the inter-trial intervals are varied and/or well-considered.

Use the ROCK to code occurrence of all (~10) target beliefs, and then let the ROCK assign sequence indices to these beliefs.

Build a table showing in which proportion of trials (either collapsed over participants or stratified in some way) prompting with the primary trial belief causes other beliefs to be mentioned in the subsequent free association period (which we'll take as evidence of the primary trial belief having activated those other beliefs).

Also, look at sequence of belief activation using designated the sequence indices.
